import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


public class login {
    public WebDriver driver;

    @BeforeMethod
    public void setupClass() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
    }



    @Test(description = "Корректный логин", priority = 0)
    public void testCorrect() {

        //Открываем на весь экран
        driver.manage().window().maximize();

        //Открываем требуемую страницу
        driver.get("http://the-internet.herokuapp.com/login");

        //Ищем поля для ввода логина и пароля
        WebElement login = driver.findElement(By.name("username"));
        WebElement password = driver.findElement(By.name("password"));

        //Вводим в поля корректные логин и пароль
        login.sendKeys("tomsmith");
        password.sendKeys("SuperSecretPassword!");

        //Ищем кнопку Login и кликаем на неё
        WebElement loginClick = driver.findElement(By.className("radius"));
        loginClick.sendKeys(Keys.ENTER);

        //Ассерт того что есть нужный лейбл
        Assert.assertTrue(driver.findElement(By.xpath("//*[contains(text(),'You logged into a secure area!')]")).isDisplayed());
    }

    @Test(description = "Некорректный логин", priority = 1)
    public void testNoCorrectLogin() {

        //Открываем на весь экран
        driver.manage().window().maximize();

        //Открываем требуемую страницу
        driver.get("http://the-internet.herokuapp.com/login");

        //Ищем поля для ввода логина
        WebElement login = driver.findElement(By.name("username"));

        //Вводим в поле некорректный логин
        login.sendKeys("tomsmith111");

        //Ищем кнопку Login и кликаем на неё
        WebElement loginClick = driver.findElement(By.className("radius"));
        loginClick.sendKeys(Keys.ENTER);

        //Ассерт того что есть нужный лейбл
        Assert.assertTrue(driver.findElement(By.xpath("//*[contains(text(),'Your username is invalid!')]")).isDisplayed());

    }

    @Test(description = "Некорректный пароль", priority = 2)
    public void testNoCorrectPassword() {

        //Открываем на весь экран
        driver.manage().window().maximize();

        //Открываем требуемую страницу
        driver.get("http://the-internet.herokuapp.com/login");

        //Ищем поля для ввода логина и пароля
        WebElement login = driver.findElement(By.name("username"));
        WebElement password = driver.findElement(By.name("password"));

        //Вводим в поля корректные логин и некорректный пароль
        login.sendKeys("tomsmith");
        password.sendKeys("123456");

        //Ищем кнопку Login и кликаем на неё
        WebElement loginClick = driver.findElement(By.className("radius"));
        loginClick.sendKeys(Keys.ENTER);

        //Ассерт того что есть нужный лейбл
        Assert.assertTrue(driver.findElement(By.xpath("//*[contains(text(),'Your password is invalid!')]")).isDisplayed());

    }

    @AfterMethod
    public void teardown() {
        if (driver != null) {
            driver.quit();
        }
    }

}
